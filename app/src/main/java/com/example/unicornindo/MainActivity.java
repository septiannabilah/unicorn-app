package com.example.unicornindo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button Gojek;
    private Button Traveloka;
    private Button Tokopedia;
    private Button BL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Gojek = (Button)findViewById(R.id.btngojek);
        Traveloka = (Button)findViewById(R.id.btntraveloka);
        Tokopedia = (Button)findViewById(R.id.btntokped);
        BL = (Button)findViewById(R.id.btnbl);

        Gojek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, GojekActivity.class);
                startActivity(intent);
            }
        });

    }
}
