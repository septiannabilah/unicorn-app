package com.example.unicornindo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class GuestBookActivity extends AppCompatActivity {

    DatabaseHelper mDatabaseHelper;
    private Button AddComment;
    private EditText Username;
    private EditText Comment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guest_book);
        AddComment = (Button)findViewById(R.id.addcomment);
        Username = (EditText)findViewById(R.id.username);
        Comment = (EditText)findViewById(R.id.comment);
        mDatabaseHelper = new DatabaseHelper(this);

        Comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userEntry = Username.getText().toString();
                String commentEntry = Comment.getText().toString();

                if (Username.length() != 0 && Comment.length() != 0){
                    AddData(userEntry, commentEntry);
                    Username.setText("");
                    Comment.setText("");
                    Intent intent = new Intent(GuestBookActivity.this, GuestListActivity.class);
                    startActivity(intent);
                } else {
                    toastMessage("You Must be Put Something in The Field");
                }
            }
        });
    }

    public void AddData(String userEntry, String commEntry){
        boolean insertData = mDatabaseHelper.addData(userEntry, commEntry);

        if (insertData){
            toastMessage("Data Succesfully Inserted");
        } else {
            toastMessage("Something Went Wrong");
        }
    }

    private void toastMessage(String message){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
