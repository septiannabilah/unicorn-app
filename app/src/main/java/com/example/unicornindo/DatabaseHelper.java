package com.example.unicornindo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.nfc.Tag;
import android.support.annotation.Nullable;
import android.util.Log;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String TABLE_NAME = "unicornDB";
    private static final String COL1 = "id";
    private static final String COL2 = "name";
    private static final String COL3 = "comment";
    private static final String TAG = "DatabaseHelper" ;

    public DatabaseHelper(Context context) {
        super(context, TABLE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTable = "CREATE TABLE " + TABLE_NAME + " (ID INTEGER PRIMARY KEY AUTOINCREMENT, " + COL2 + " TEXT)";
        db.execSQL(createTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP IF TABLE EXIST " + TABLE_NAME);
        onCreate(db);
    }

    public boolean addData(String user, String comm){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL2, user);

        ContentValues contentValues1 = new ContentValues();
        contentValues1.put(COL3, comm);

        Log.d(TAG, "addData : Adding " + user + " to " + TABLE_NAME);

        Log.d(TAG, "addData : Adding " + comm + " to " + TABLE_NAME);

        long result = db.insert(TABLE_NAME, null, contentValues);

        long result1 = db.insert(TABLE_NAME, null, contentValues1);

        if (result == -1 || result1 == -1)
            return false;
        else
            return true;
    }

    public Cursor getData(){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_NAME;
        Cursor data = db.rawQuery(query, null);
        return data;
    }
}
