package com.example.unicornindo;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class GojekActivity extends AppCompatActivity {

    private Button Gallery;
    private Button Desc;
    private Button Loc;
    private Button Guest;
    private Button Contact;
    private Button Web;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gojek);

        Loc = (Button)findViewById(R.id.loc);
        Gallery = (Button)findViewById(R.id.gallery);
        Desc = (Button)findViewById(R.id.desc);
        Guest = (Button)findViewById(R.id.guest);
        Contact = (Button)findViewById(R.id.contact);
        Web = (Button)findViewById(R.id.web);

        Loc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainMenu(v);
            }
        });
        Contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainMenu(v);
            }
        });
        Web.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainMenu(v);
            }
        });
        Gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainMenu(v);
            }
        });
        Desc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainMenu(v);
            }
        });
        Guest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainMenu(v);
            }
        });

    }

    private void mainMenu(View view){
        Intent intent = null;
        switch (view.getId()){
            case R.id.contact:
                intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:02150251110"));
                startActivity(intent);
                break;
            case R.id.web:
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.go-jek.com/"));
                startActivity(intent);
                break;
            case R.id.gallery:
                intent = new Intent(GojekActivity.this, GalleryActivity.class);
                startActivity(intent);
                break;
            case R.id.desc:
                intent = new Intent(GojekActivity.this, DescriptionActivity.class);
                startActivity(intent);
                break;
            case R.id.loc:
                intent = new Intent(GojekActivity.this, MapsActivity.class);
                startActivity(intent);
                break;
            case R.id.guest:
                intent = new Intent(GojekActivity.this, GuestListActivity.class);
                startActivity(intent);
                break;

        }
    }
}
