package com.example.unicornindo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;

public class ContactActivity extends AppCompatActivity {

    private EditText TelpGojek;
    private Button Dial;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        try{
            setContentView(R.layout.activity_contact);
            TelpGojek = (EditText)findViewById(R.id.telpgojek);
            Dial = (Button)findViewById(R.id.dial);
            Dial.setOnClickListener(new ClickHandler());
        }
        catch(Exception e){
            Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private class ClickHandler implements OnClickListener{
        @Override
        public void onClick(View v){
            try{
                String myData = TelpGojek.getText().toString();
                Intent myActivity2 = new Intent(Intent.ACTION_DIAL, Uri.parse(myData));
                startActivity(myActivity2);
            }
            catch (Exception e){
                Toast.makeText(getBaseContext(),e.getMessage(), Toast.LENGTH_LONG).show();
            }
        }
    }



}
